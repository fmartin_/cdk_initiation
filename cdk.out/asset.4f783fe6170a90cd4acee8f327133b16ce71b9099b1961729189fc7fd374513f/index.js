exports.handler = async (event) => {
    try {

        const statusCode = Number(event?.queryStringParameters?.code);
        // const statusCode = "200";
        
        const response = {
            statusCode,
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET",
                "Access-Control-Allow-Headers": "application/json"
            }
        };
        
    
        switch(statusCode) {
            case 200:
                response.body = JSON.stringify({
                    code: statusCode,
                    message: "Tout est fonctionnel mon commandant !!"
                });
                return response;
    
            case 400:
                response.body = JSON.stringify({
                    code: statusCode,
                    message: "Franchement pas folle la 400..."
                });
                return response;
    
            case 500:
                throw new ErrorResponse();
        }
        
    } catch (error) {
        let err = error;
        
        if (!err instanceof ErrorResponse) {
            err = new ErrorResponse();
        }
        return err;
    }
};
    
class ErrorResponse {
    constructor(message = "Pas mal la 500 !", statusCode = 500) {
        this.statusCode = statusCode;
        
        (this.body = JSON.stringify({ message, code: statusCode })),
            (this.headers = {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET",
                "Access-Control-Allow-Headers": "application/json",
            });
    }
}