async function main(event) {
    try {

        // const statusCode = Number(event?.queryStringParameters?.code);
        const statusCode = 200;
        
        const response = {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET",
                "Access-Control-Allow-Headers": "application/json"
            }
        };
        
        console.log("statusCode:", statusCode);
    
        switch(statusCode) {
            case 200:
                response.body = JSON.stringify({
                    message: "Tout est fonctionnel mon commandant !!",
                    code: statusCode
                    });
                return response;
    
            case 400:
                response.body = JSON.stringify({
                    message: "Franchement pas folle la 400...",
                    code: statusCode
                    });
                return response;
    
            case 500:
                throw new ErrorResponse();
        }
        
    } catch (error) {
        let err = error;
        
        if (!err instanceof ErrorResponse) {
            err = new ErrorResponse();
        }
        return err;
    }
};
    
class ErrorResponse {
    constructor(message = "Pas mal la 500 !", statusCode = 500) {
        this.statusCode = statusCode;
        
        (this.body = JSON.stringify({ message, code: statusCode })),
            (this.headers = {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET",
                "Access-Control-Allow-Headers": "application/json",
            });
    }
}