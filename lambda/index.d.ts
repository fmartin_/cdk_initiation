interface IResponse {
    statusCode: number;
    body: string;
    headers: {
        [key: string]: string;
    };
}
export declare function handler(event: {
    queryStringParameters: {
        code: any;
    };
}): Promise<IResponse>;
export {};
