interface IResponse {
    statusCode: number;
    body: string;
    headers: {
        [key: string]: string;
    };
}

export async function handler(event: { queryStringParameters: { code: any; }; }) {
    // Get the status code from the query string parameter
    var statusCode = event?.queryStringParameters?.code;
    
    const response: IResponse = {
        statusCode,
        body: "",
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET",
            "Access-Control-Allow-Headers": "application/json"
        },
    };

    if (statusCode) {
        // Convert the status code to a number if possible
        if (!!Number(statusCode)) {
            statusCode = Number(statusCode);
        } else {
            response.statusCode = 400;
            response.body = JSON.stringify({ message: "Bad request" });
            return response;
        }
    
        // Return a response with the specified status code
        switch(statusCode) {
            case 200:
                response.body = JSON.stringify({message: "Tout est fonctionnel mon commandant !!"});
                return response;
    
            case 400:
                response.body = JSON.stringify({message: "Franchement pas folle la 400..."});
                return response;
    
            case 500:
                response.body = JSON.stringify({message: "Pas mal la nouvelle 500 !"});
                return response;
    
            default:
                response.statusCode = 400;
                response.body = JSON.stringify({message: "L'API n'accepte que les codes 200, 400 et 500..."});
                return response;
        }
    } else {
        // If there is no status code, return a 400 response
        response.statusCode = 400;
        response.body = JSON.stringify({message: "No status code provided"});
        return response;
    }
};