import * as acm from "aws-cdk-lib/aws-certificatemanager";
import * as apigateway from 'aws-cdk-lib/aws-apigateway';
import * as cloudfront from 'aws-cdk-lib/aws-cloudfront';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as route53 from 'aws-cdk-lib/aws-route53';
import * as s3Deploy from 'aws-cdk-lib/aws-s3-deployment';
import * as targets from 'aws-cdk-lib/aws-route53-targets';

import { RemovalPolicy, Stack, StackProps } from 'aws-cdk-lib';

import { Construct } from 'constructs';
import { aws_s3 as s3 } from 'aws-cdk-lib';

export class CdkInitStack extends Stack {
    constructor(scope: Construct, id: string, props?: StackProps) {
        super(scope, id, props);

        const domainName: string = 'fma.d.kodehyve.com';
        const subDomainName: string = 'custom.fma.d.kodehyve.com';
        const apiSubDomainName: string = 'api.custom.fma.d.kodehyve.com';

        // Create the public S3 bucket
        const s3bucket = new s3.Bucket(this, 'MyCDKBucket', {
            publicReadAccess: true, // enable public read access
            removalPolicy: RemovalPolicy.DESTROY,
            autoDeleteObjects: true,
            websiteIndexDocument: 'index.html' // set index.html as the default file
        });

        // Static Code in to Bucket
        new s3Deploy.BucketDeployment(this, 'DeployWebsite', {
            sources: [s3Deploy.Source.asset('./static')], // deploy the contents of the ./static directory
            destinationBucket: s3bucket // deploy to the above bucket
        });

        // Get the hosted zone for the domain
        const hostedZone = route53.HostedZone.fromLookup(this, 'HostedZone', { domainName });

        // Create a certificate
        const certificate = new acm.DnsValidatedCertificate(this, 'MyCertificate', {
            domainName: `*.${domainName}`,
            hostedZone,
            region: 'us-east-1' // certificate creation is only valid in us-east-1
        });

        const viewCertificate = cloudfront.ViewerCertificate.fromAcmCertificate(certificate, {
            aliases: [subDomainName], // set the alternate domain name to the certificate
            securityPolicy: cloudfront.SecurityPolicyProtocol.TLS_V1_2_2018,
            sslMethod: cloudfront.SSLMethod.SNI
        });

        // CloudFront distribution
        const distribution = new cloudfront.CloudFrontWebDistribution(this, 'MyDistribution', {
            defaultRootObject: 'index.html', // default file
            viewerCertificate: viewCertificate, // set the certificate
            originConfigs: [
                {
                    s3OriginSource: { s3BucketSource: s3bucket }, // set the bucket
                    behaviors: [{
                        isDefaultBehavior: true,
                        allowedMethods: cloudfront.CloudFrontAllowedMethods.ALL
                    }]
                }
            ]
        });

        // Route 53 alias record for the CloudFront distribution
        new route53.ARecord(this, 'ARecord', {
            recordName: subDomainName, // set the Record name
            target: route53.RecordTarget.fromAlias(new targets.CloudFrontTarget(distribution)), // set the value to the cloudfront distribution
            zone: hostedZone // set the hosted zone
        });

// =========== LAMBDA + REST API ===========

        // Lambda function
        const lambdaFunction = new lambda.Function(this, 'MyLambdaFunction', {
            runtime: lambda.Runtime.NODEJS_14_X, // set the runtime
            code: lambda.Code.fromAsset('./lambda'), // set the code
            handler: 'index.handler', // set the handler
            environment: {
                REGION: 'eu-west-1',
                BUCKET_NAME: s3bucket.bucketName
            }
        });

        // Certificate for the Lambda function
        const certificateLambda = new acm.DnsValidatedCertificate(this, 'MyCertificateLambda', {
            domainName: `${apiSubDomainName}`,
            hostedZone
        });

        // Rest API (CORS enabled)
        const restApiGateway = new apigateway.RestApi(this, 'MyRestApi', {
            restApiName: 'MyRestApi', // set the rest api name
            domainName: {
                domainName: `${apiSubDomainName}`,
                certificate: certificateLambda,
                securityPolicy: apigateway.SecurityPolicy.TLS_1_2,
            },
            defaultCorsPreflightOptions: {
                allowOrigins: apigateway.Cors.ALL_ORIGINS,
                allowMethods: apigateway.Cors.ALL_METHODS,
                allowHeaders: apigateway.Cors.DEFAULT_HEADERS
            }
        });

        // Rest API method
        restApiGateway.root.addMethod('GET', new apigateway.LambdaIntegration(lambdaFunction));

        // Route 53 alias record for the Rest API
        new route53.ARecord(this, 'ARecordLambda', {
            recordName: apiSubDomainName,
            target: route53.RecordTarget.fromAlias(new targets.ApiGateway(restApiGateway)),
            zone: hostedZone
        });
    }
}