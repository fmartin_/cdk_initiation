"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CdkInitStack = void 0;
const acm = require("aws-cdk-lib/aws-certificatemanager");
const apigateway = require("aws-cdk-lib/aws-apigateway");
const cloudfront = require("aws-cdk-lib/aws-cloudfront");
const lambda = require("aws-cdk-lib/aws-lambda");
const route53 = require("aws-cdk-lib/aws-route53");
const s3Deploy = require("aws-cdk-lib/aws-s3-deployment");
const targets = require("aws-cdk-lib/aws-route53-targets");
const aws_cdk_lib_1 = require("aws-cdk-lib");
const aws_cdk_lib_2 = require("aws-cdk-lib");
class CdkInitStack extends aws_cdk_lib_1.Stack {
    constructor(scope, id, props) {
        super(scope, id, props);
        const domainName = 'fma.d.kodehyve.com';
        const subDomainName = 'custom.fma.d.kodehyve.com';
        const apiSubDomainName = 'api.custom.fma.d.kodehyve.com';
        // Create the public S3 bucket
        const s3bucket = new aws_cdk_lib_2.aws_s3.Bucket(this, 'MyCDKBucket', {
            publicReadAccess: true,
            removalPolicy: aws_cdk_lib_1.RemovalPolicy.DESTROY,
            autoDeleteObjects: true,
            websiteIndexDocument: 'index.html' // set index.html as the default file
        });
        // Static Code in to Bucket
        new s3Deploy.BucketDeployment(this, 'DeployWebsite', {
            sources: [s3Deploy.Source.asset('./static')],
            destinationBucket: s3bucket // deploy to the above bucket
        });
        // Get the hosted zone for the domain
        const hostedZone = route53.HostedZone.fromLookup(this, 'HostedZone', { domainName });
        // Create a certificate
        const certificate = new acm.DnsValidatedCertificate(this, 'MyCertificate', {
            domainName: `*.${domainName}`,
            hostedZone,
            region: 'us-east-1' // certificate creation is only valid in us-east-1
        });
        const viewCertificate = cloudfront.ViewerCertificate.fromAcmCertificate(certificate, {
            aliases: [subDomainName],
            securityPolicy: cloudfront.SecurityPolicyProtocol.TLS_V1_2_2018,
            sslMethod: cloudfront.SSLMethod.SNI
        });
        // CloudFront distribution
        const distribution = new cloudfront.CloudFrontWebDistribution(this, 'MyDistribution', {
            defaultRootObject: 'index.html',
            viewerCertificate: viewCertificate,
            originConfigs: [
                {
                    s3OriginSource: { s3BucketSource: s3bucket },
                    behaviors: [{
                            isDefaultBehavior: true,
                            allowedMethods: cloudfront.CloudFrontAllowedMethods.ALL
                        }]
                }
            ]
        });
        // Route 53 alias record for the CloudFront distribution
        new route53.ARecord(this, 'ARecord', {
            recordName: subDomainName,
            target: route53.RecordTarget.fromAlias(new targets.CloudFrontTarget(distribution)),
            zone: hostedZone // set the hosted zone
        });
        // =========== LAMBDA + REST API ===========
        // Lambda function
        const lambdaFunction = new lambda.Function(this, 'MyLambdaFunction', {
            runtime: lambda.Runtime.NODEJS_14_X,
            code: lambda.Code.fromAsset('./lambda'),
            handler: 'index.handler',
            environment: {
                REGION: 'eu-west-1',
                BUCKET_NAME: s3bucket.bucketName
            }
        });
        // Certificate for the Lambda function
        const certificateLambda = new acm.DnsValidatedCertificate(this, 'MyCertificateLambda', {
            domainName: `${apiSubDomainName}`,
            hostedZone
        });
        // Rest API (CORS enabled)
        const restApiGateway = new apigateway.RestApi(this, 'MyRestApi', {
            restApiName: 'MyRestApi',
            domainName: {
                domainName: `${apiSubDomainName}`,
                certificate: certificateLambda,
                securityPolicy: apigateway.SecurityPolicy.TLS_1_2,
            },
            defaultCorsPreflightOptions: {
                allowOrigins: apigateway.Cors.ALL_ORIGINS,
                allowMethods: apigateway.Cors.ALL_METHODS,
                allowHeaders: apigateway.Cors.DEFAULT_HEADERS
            }
        });
        // Rest API method
        restApiGateway.root.addMethod('GET', new apigateway.LambdaIntegration(lambdaFunction));
        new route53.ARecord(this, 'ARecordLambda', {
            recordName: apiSubDomainName,
            target: route53.RecordTarget.fromAlias(new targets.ApiGateway(restApiGateway)),
            zone: hostedZone
        });
    }
}
exports.CdkInitStack = CdkInitStack;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2RrX2luaXQtc3RhY2suanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjZGtfaW5pdC1zdGFjay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSwwREFBMEQ7QUFDMUQseURBQXlEO0FBQ3pELHlEQUF5RDtBQUN6RCxpREFBaUQ7QUFDakQsbURBQW1EO0FBQ25ELDBEQUEwRDtBQUMxRCwyREFBMkQ7QUFFM0QsNkNBQStEO0FBRy9ELDZDQUEyQztBQUUzQyxNQUFhLFlBQWEsU0FBUSxtQkFBSztJQUNuQyxZQUFZLEtBQWdCLEVBQUUsRUFBVSxFQUFFLEtBQWtCO1FBQ3hELEtBQUssQ0FBQyxLQUFLLEVBQUUsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBRXhCLE1BQU0sVUFBVSxHQUFXLG9CQUFvQixDQUFDO1FBQ2hELE1BQU0sYUFBYSxHQUFXLDJCQUEyQixDQUFDO1FBQzFELE1BQU0sZ0JBQWdCLEdBQVcsK0JBQStCLENBQUM7UUFFakUsOEJBQThCO1FBQzlCLE1BQU0sUUFBUSxHQUFHLElBQUksb0JBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLGFBQWEsRUFBRTtZQUNoRCxnQkFBZ0IsRUFBRSxJQUFJO1lBQ3RCLGFBQWEsRUFBRSwyQkFBYSxDQUFDLE9BQU87WUFDcEMsaUJBQWlCLEVBQUUsSUFBSTtZQUN2QixvQkFBb0IsRUFBRSxZQUFZLENBQUMscUNBQXFDO1NBQzNFLENBQUMsQ0FBQztRQUVILDJCQUEyQjtRQUMzQixJQUFJLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsZUFBZSxFQUFFO1lBQ2pELE9BQU8sRUFBRSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzVDLGlCQUFpQixFQUFFLFFBQVEsQ0FBQyw2QkFBNkI7U0FDNUQsQ0FBQyxDQUFDO1FBRUgscUNBQXFDO1FBQ3JDLE1BQU0sVUFBVSxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxZQUFZLEVBQUUsRUFBRSxVQUFVLEVBQUUsQ0FBQyxDQUFDO1FBRXJGLHVCQUF1QjtRQUN2QixNQUFNLFdBQVcsR0FBRyxJQUFJLEdBQUcsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLEVBQUUsZUFBZSxFQUFFO1lBQ3ZFLFVBQVUsRUFBRSxLQUFLLFVBQVUsRUFBRTtZQUM3QixVQUFVO1lBQ1YsTUFBTSxFQUFFLFdBQVcsQ0FBQyxrREFBa0Q7U0FDekUsQ0FBQyxDQUFDO1FBRUgsTUFBTSxlQUFlLEdBQUcsVUFBVSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRTtZQUNqRixPQUFPLEVBQUUsQ0FBQyxhQUFhLENBQUM7WUFDeEIsY0FBYyxFQUFFLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhO1lBQy9ELFNBQVMsRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUc7U0FDdEMsQ0FBQyxDQUFDO1FBRUgsMEJBQTBCO1FBQzFCLE1BQU0sWUFBWSxHQUFHLElBQUksVUFBVSxDQUFDLHlCQUF5QixDQUFDLElBQUksRUFBRSxnQkFBZ0IsRUFBRTtZQUNsRixpQkFBaUIsRUFBRSxZQUFZO1lBQy9CLGlCQUFpQixFQUFFLGVBQWU7WUFDbEMsYUFBYSxFQUFFO2dCQUNYO29CQUNJLGNBQWMsRUFBRSxFQUFFLGNBQWMsRUFBRSxRQUFRLEVBQUU7b0JBQzVDLFNBQVMsRUFBRSxDQUFDOzRCQUNSLGlCQUFpQixFQUFFLElBQUk7NEJBQ3ZCLGNBQWMsRUFBRSxVQUFVLENBQUMsd0JBQXdCLENBQUMsR0FBRzt5QkFDMUQsQ0FBQztpQkFDTDthQUNKO1NBQ0osQ0FBQyxDQUFDO1FBRUgsd0RBQXdEO1FBQ3hELElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsU0FBUyxFQUFFO1lBQ2pDLFVBQVUsRUFBRSxhQUFhO1lBQ3pCLE1BQU0sRUFBRSxPQUFPLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNsRixJQUFJLEVBQUUsVUFBVSxDQUFDLHNCQUFzQjtTQUMxQyxDQUFDLENBQUM7UUFFWCw0Q0FBNEM7UUFFcEMsa0JBQWtCO1FBQ2xCLE1BQU0sY0FBYyxHQUFHLElBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsa0JBQWtCLEVBQUU7WUFDakUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsV0FBVztZQUNuQyxJQUFJLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDO1lBQ3ZDLE9BQU8sRUFBRSxlQUFlO1lBQ3hCLFdBQVcsRUFBRTtnQkFDVCxNQUFNLEVBQUUsV0FBVztnQkFDbkIsV0FBVyxFQUFFLFFBQVEsQ0FBQyxVQUFVO2FBQ25DO1NBQ0osQ0FBQyxDQUFDO1FBRUgsc0NBQXNDO1FBQ3RDLE1BQU0saUJBQWlCLEdBQUcsSUFBSSxHQUFHLENBQUMsdUJBQXVCLENBQUMsSUFBSSxFQUFFLHFCQUFxQixFQUFFO1lBQ25GLFVBQVUsRUFBRSxHQUFHLGdCQUFnQixFQUFFO1lBQ2pDLFVBQVU7U0FDYixDQUFDLENBQUM7UUFFSCwwQkFBMEI7UUFDMUIsTUFBTSxjQUFjLEdBQUcsSUFBSSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxXQUFXLEVBQUU7WUFDN0QsV0FBVyxFQUFFLFdBQVc7WUFDeEIsVUFBVSxFQUFFO2dCQUNSLFVBQVUsRUFBRSxHQUFHLGdCQUFnQixFQUFFO2dCQUNqQyxXQUFXLEVBQUUsaUJBQWlCO2dCQUM5QixjQUFjLEVBQUUsVUFBVSxDQUFDLGNBQWMsQ0FBQyxPQUFPO2FBQ3BEO1lBQ0QsMkJBQTJCLEVBQUU7Z0JBQ3pCLFlBQVksRUFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVc7Z0JBQ3pDLFlBQVksRUFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVc7Z0JBQ3pDLFlBQVksRUFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLGVBQWU7YUFDaEQ7U0FDSixDQUFDLENBQUM7UUFFSCxrQkFBa0I7UUFDbEIsY0FBYyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLElBQUksVUFBVSxDQUFDLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7UUFFdkYsSUFBSSxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxlQUFlLEVBQUU7WUFDdkMsVUFBVSxFQUFFLGdCQUFnQjtZQUM1QixNQUFNLEVBQUUsT0FBTyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsSUFBSSxPQUFPLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQzlFLElBQUksRUFBRSxVQUFVO1NBQ25CLENBQUMsQ0FBQztJQUNQLENBQUM7Q0FDSjtBQXZHRCxvQ0F1R0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBhY20gZnJvbSBcImF3cy1jZGstbGliL2F3cy1jZXJ0aWZpY2F0ZW1hbmFnZXJcIjtcbmltcG9ydCAqIGFzIGFwaWdhdGV3YXkgZnJvbSAnYXdzLWNkay1saWIvYXdzLWFwaWdhdGV3YXknO1xuaW1wb3J0ICogYXMgY2xvdWRmcm9udCBmcm9tICdhd3MtY2RrLWxpYi9hd3MtY2xvdWRmcm9udCc7XG5pbXBvcnQgKiBhcyBsYW1iZGEgZnJvbSAnYXdzLWNkay1saWIvYXdzLWxhbWJkYSc7XG5pbXBvcnQgKiBhcyByb3V0ZTUzIGZyb20gJ2F3cy1jZGstbGliL2F3cy1yb3V0ZTUzJztcbmltcG9ydCAqIGFzIHMzRGVwbG95IGZyb20gJ2F3cy1jZGstbGliL2F3cy1zMy1kZXBsb3ltZW50JztcbmltcG9ydCAqIGFzIHRhcmdldHMgZnJvbSAnYXdzLWNkay1saWIvYXdzLXJvdXRlNTMtdGFyZ2V0cyc7XG5cbmltcG9ydCB7IFJlbW92YWxQb2xpY3ksIFN0YWNrLCBTdGFja1Byb3BzIH0gZnJvbSAnYXdzLWNkay1saWInO1xuXG5pbXBvcnQgeyBDb25zdHJ1Y3QgfSBmcm9tICdjb25zdHJ1Y3RzJztcbmltcG9ydCB7IGF3c19zMyBhcyBzMyB9IGZyb20gJ2F3cy1jZGstbGliJztcblxuZXhwb3J0IGNsYXNzIENka0luaXRTdGFjayBleHRlbmRzIFN0YWNrIHtcbiAgICBjb25zdHJ1Y3RvcihzY29wZTogQ29uc3RydWN0LCBpZDogc3RyaW5nLCBwcm9wcz86IFN0YWNrUHJvcHMpIHtcbiAgICAgICAgc3VwZXIoc2NvcGUsIGlkLCBwcm9wcyk7XG5cbiAgICAgICAgY29uc3QgZG9tYWluTmFtZTogc3RyaW5nID0gJ2ZtYS5kLmtvZGVoeXZlLmNvbSc7XG4gICAgICAgIGNvbnN0IHN1YkRvbWFpbk5hbWU6IHN0cmluZyA9ICdjdXN0b20uZm1hLmQua29kZWh5dmUuY29tJztcbiAgICAgICAgY29uc3QgYXBpU3ViRG9tYWluTmFtZTogc3RyaW5nID0gJ2FwaS5jdXN0b20uZm1hLmQua29kZWh5dmUuY29tJztcblxuICAgICAgICAvLyBDcmVhdGUgdGhlIHB1YmxpYyBTMyBidWNrZXRcbiAgICAgICAgY29uc3QgczNidWNrZXQgPSBuZXcgczMuQnVja2V0KHRoaXMsICdNeUNES0J1Y2tldCcsIHtcbiAgICAgICAgICAgIHB1YmxpY1JlYWRBY2Nlc3M6IHRydWUsIC8vIGVuYWJsZSBwdWJsaWMgcmVhZCBhY2Nlc3NcbiAgICAgICAgICAgIHJlbW92YWxQb2xpY3k6IFJlbW92YWxQb2xpY3kuREVTVFJPWSxcbiAgICAgICAgICAgIGF1dG9EZWxldGVPYmplY3RzOiB0cnVlLFxuICAgICAgICAgICAgd2Vic2l0ZUluZGV4RG9jdW1lbnQ6ICdpbmRleC5odG1sJyAvLyBzZXQgaW5kZXguaHRtbCBhcyB0aGUgZGVmYXVsdCBmaWxlXG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vIFN0YXRpYyBDb2RlIGluIHRvIEJ1Y2tldFxuICAgICAgICBuZXcgczNEZXBsb3kuQnVja2V0RGVwbG95bWVudCh0aGlzLCAnRGVwbG95V2Vic2l0ZScsIHtcbiAgICAgICAgICAgIHNvdXJjZXM6IFtzM0RlcGxveS5Tb3VyY2UuYXNzZXQoJy4vc3RhdGljJyldLCAvLyBkZXBsb3kgdGhlIGNvbnRlbnRzIG9mIHRoZSAuL3N0YXRpYyBkaXJlY3RvcnlcbiAgICAgICAgICAgIGRlc3RpbmF0aW9uQnVja2V0OiBzM2J1Y2tldCAvLyBkZXBsb3kgdG8gdGhlIGFib3ZlIGJ1Y2tldFxuICAgICAgICB9KTtcblxuICAgICAgICAvLyBHZXQgdGhlIGhvc3RlZCB6b25lIGZvciB0aGUgZG9tYWluXG4gICAgICAgIGNvbnN0IGhvc3RlZFpvbmUgPSByb3V0ZTUzLkhvc3RlZFpvbmUuZnJvbUxvb2t1cCh0aGlzLCAnSG9zdGVkWm9uZScsIHsgZG9tYWluTmFtZSB9KTtcblxuICAgICAgICAvLyBDcmVhdGUgYSBjZXJ0aWZpY2F0ZVxuICAgICAgICBjb25zdCBjZXJ0aWZpY2F0ZSA9IG5ldyBhY20uRG5zVmFsaWRhdGVkQ2VydGlmaWNhdGUodGhpcywgJ015Q2VydGlmaWNhdGUnLCB7XG4gICAgICAgICAgICBkb21haW5OYW1lOiBgKi4ke2RvbWFpbk5hbWV9YCxcbiAgICAgICAgICAgIGhvc3RlZFpvbmUsXG4gICAgICAgICAgICByZWdpb246ICd1cy1lYXN0LTEnIC8vIGNlcnRpZmljYXRlIGNyZWF0aW9uIGlzIG9ubHkgdmFsaWQgaW4gdXMtZWFzdC0xXG4gICAgICAgIH0pO1xuXG4gICAgICAgIGNvbnN0IHZpZXdDZXJ0aWZpY2F0ZSA9IGNsb3VkZnJvbnQuVmlld2VyQ2VydGlmaWNhdGUuZnJvbUFjbUNlcnRpZmljYXRlKGNlcnRpZmljYXRlLCB7XG4gICAgICAgICAgICBhbGlhc2VzOiBbc3ViRG9tYWluTmFtZV0sIC8vIHNldCB0aGUgYWx0ZXJuYXRlIGRvbWFpbiBuYW1lIHRvIHRoZSBjZXJ0aWZpY2F0ZVxuICAgICAgICAgICAgc2VjdXJpdHlQb2xpY3k6IGNsb3VkZnJvbnQuU2VjdXJpdHlQb2xpY3lQcm90b2NvbC5UTFNfVjFfMl8yMDE4LFxuICAgICAgICAgICAgc3NsTWV0aG9kOiBjbG91ZGZyb250LlNTTE1ldGhvZC5TTklcbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gQ2xvdWRGcm9udCBkaXN0cmlidXRpb25cbiAgICAgICAgY29uc3QgZGlzdHJpYnV0aW9uID0gbmV3IGNsb3VkZnJvbnQuQ2xvdWRGcm9udFdlYkRpc3RyaWJ1dGlvbih0aGlzLCAnTXlEaXN0cmlidXRpb24nLCB7XG4gICAgICAgICAgICBkZWZhdWx0Um9vdE9iamVjdDogJ2luZGV4Lmh0bWwnLCAvLyBkZWZhdWx0IGZpbGVcbiAgICAgICAgICAgIHZpZXdlckNlcnRpZmljYXRlOiB2aWV3Q2VydGlmaWNhdGUsIC8vIHNldCB0aGUgY2VydGlmaWNhdGVcbiAgICAgICAgICAgIG9yaWdpbkNvbmZpZ3M6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIHMzT3JpZ2luU291cmNlOiB7IHMzQnVja2V0U291cmNlOiBzM2J1Y2tldCB9LCAvLyBzZXQgdGhlIGJ1Y2tldFxuICAgICAgICAgICAgICAgICAgICBiZWhhdmlvcnM6IFt7XG4gICAgICAgICAgICAgICAgICAgICAgICBpc0RlZmF1bHRCZWhhdmlvcjogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsbG93ZWRNZXRob2RzOiBjbG91ZGZyb250LkNsb3VkRnJvbnRBbGxvd2VkTWV0aG9kcy5BTExcbiAgICAgICAgICAgICAgICAgICAgfV1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vIFJvdXRlIDUzIGFsaWFzIHJlY29yZCBmb3IgdGhlIENsb3VkRnJvbnQgZGlzdHJpYnV0aW9uXG4gICAgICAgIG5ldyByb3V0ZTUzLkFSZWNvcmQodGhpcywgJ0FSZWNvcmQnLCB7XG4gICAgICAgICAgICByZWNvcmROYW1lOiBzdWJEb21haW5OYW1lLCAvLyBzZXQgdGhlIFJlY29yZCBuYW1lXG4gICAgICAgICAgICB0YXJnZXQ6IHJvdXRlNTMuUmVjb3JkVGFyZ2V0LmZyb21BbGlhcyhuZXcgdGFyZ2V0cy5DbG91ZEZyb250VGFyZ2V0KGRpc3RyaWJ1dGlvbikpLCAvLyBzZXQgdGhlIHZhbHVlIHRvIHRoZSBjbG91ZGZyb250IGRpc3RyaWJ1dGlvblxuICAgICAgICAgICAgem9uZTogaG9zdGVkWm9uZSAvLyBzZXQgdGhlIGhvc3RlZCB6b25lXG4gICAgICAgIH0pO1xuXG4vLyA9PT09PT09PT09PSBMQU1CREEgKyBSRVNUIEFQSSA9PT09PT09PT09PVxuXG4gICAgICAgIC8vIExhbWJkYSBmdW5jdGlvblxuICAgICAgICBjb25zdCBsYW1iZGFGdW5jdGlvbiA9IG5ldyBsYW1iZGEuRnVuY3Rpb24odGhpcywgJ015TGFtYmRhRnVuY3Rpb24nLCB7XG4gICAgICAgICAgICBydW50aW1lOiBsYW1iZGEuUnVudGltZS5OT0RFSlNfMTRfWCwgLy8gc2V0IHRoZSBydW50aW1lXG4gICAgICAgICAgICBjb2RlOiBsYW1iZGEuQ29kZS5mcm9tQXNzZXQoJy4vbGFtYmRhJyksIC8vIHNldCB0aGUgY29kZVxuICAgICAgICAgICAgaGFuZGxlcjogJ2luZGV4LmhhbmRsZXInLCAvLyBzZXQgdGhlIGhhbmRsZXJcbiAgICAgICAgICAgIGVudmlyb25tZW50OiB7XG4gICAgICAgICAgICAgICAgUkVHSU9OOiAnZXUtd2VzdC0xJyxcbiAgICAgICAgICAgICAgICBCVUNLRVRfTkFNRTogczNidWNrZXQuYnVja2V0TmFtZVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICAvLyBDZXJ0aWZpY2F0ZSBmb3IgdGhlIExhbWJkYSBmdW5jdGlvblxuICAgICAgICBjb25zdCBjZXJ0aWZpY2F0ZUxhbWJkYSA9IG5ldyBhY20uRG5zVmFsaWRhdGVkQ2VydGlmaWNhdGUodGhpcywgJ015Q2VydGlmaWNhdGVMYW1iZGEnLCB7XG4gICAgICAgICAgICBkb21haW5OYW1lOiBgJHthcGlTdWJEb21haW5OYW1lfWAsXG4gICAgICAgICAgICBob3N0ZWRab25lXG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vIFJlc3QgQVBJIChDT1JTIGVuYWJsZWQpXG4gICAgICAgIGNvbnN0IHJlc3RBcGlHYXRld2F5ID0gbmV3IGFwaWdhdGV3YXkuUmVzdEFwaSh0aGlzLCAnTXlSZXN0QXBpJywge1xuICAgICAgICAgICAgcmVzdEFwaU5hbWU6ICdNeVJlc3RBcGknLCAvLyBzZXQgdGhlIHJlc3QgYXBpIG5hbWVcbiAgICAgICAgICAgIGRvbWFpbk5hbWU6IHtcbiAgICAgICAgICAgICAgICBkb21haW5OYW1lOiBgJHthcGlTdWJEb21haW5OYW1lfWAsXG4gICAgICAgICAgICAgICAgY2VydGlmaWNhdGU6IGNlcnRpZmljYXRlTGFtYmRhLFxuICAgICAgICAgICAgICAgIHNlY3VyaXR5UG9saWN5OiBhcGlnYXRld2F5LlNlY3VyaXR5UG9saWN5LlRMU18xXzIsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZGVmYXVsdENvcnNQcmVmbGlnaHRPcHRpb25zOiB7XG4gICAgICAgICAgICAgICAgYWxsb3dPcmlnaW5zOiBhcGlnYXRld2F5LkNvcnMuQUxMX09SSUdJTlMsXG4gICAgICAgICAgICAgICAgYWxsb3dNZXRob2RzOiBhcGlnYXRld2F5LkNvcnMuQUxMX01FVEhPRFMsXG4gICAgICAgICAgICAgICAgYWxsb3dIZWFkZXJzOiBhcGlnYXRld2F5LkNvcnMuREVGQVVMVF9IRUFERVJTXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vIFJlc3QgQVBJIG1ldGhvZFxuICAgICAgICByZXN0QXBpR2F0ZXdheS5yb290LmFkZE1ldGhvZCgnR0VUJywgbmV3IGFwaWdhdGV3YXkuTGFtYmRhSW50ZWdyYXRpb24obGFtYmRhRnVuY3Rpb24pKTtcblxuICAgICAgICBuZXcgcm91dGU1My5BUmVjb3JkKHRoaXMsICdBUmVjb3JkTGFtYmRhJywge1xuICAgICAgICAgICAgcmVjb3JkTmFtZTogYXBpU3ViRG9tYWluTmFtZSxcbiAgICAgICAgICAgIHRhcmdldDogcm91dGU1My5SZWNvcmRUYXJnZXQuZnJvbUFsaWFzKG5ldyB0YXJnZXRzLkFwaUdhdGV3YXkocmVzdEFwaUdhdGV3YXkpKSxcbiAgICAgICAgICAgIHpvbmU6IGhvc3RlZFpvbmVcbiAgICAgICAgfSk7XG4gICAgfVxufSJdfQ==