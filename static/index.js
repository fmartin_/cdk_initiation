const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

fetch(`https://api.custom.fma.d.kodehyve.com/?code=${urlParams.get('code')}`)
    .then(response => response.json())
    .then(data => {
        console.log("DATA", data);
        
        document.getElementById("response").innerHTML = data.message ?? "Aucune donnée...";
    })
    .catch(error => { console.log(error); }
);